package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import java.io.File;
import java.util.Objects;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void shareVideo(String filePath){
        File videoFile =new File(filePath);
        Uri videoURI ;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            videoURI =  FileProvider.getUriForFile(
                    Objects.requireNonNull(getApplicationContext()),
                    BuildConfig.APPLICATION_ID + ".provider", videoFile
            );
        else
            videoURI=   Uri.fromFile(videoFile);


        ShareCompat.IntentBuilder.from(this)
                .setStream(videoURI)
                .setType("video/mp4")
                .setChooserTitle("Share video...")
                .startChooser();

    }
}